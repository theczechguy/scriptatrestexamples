function Handler($context, $inputs) {
    $paramObject = New-Object -TypeName psobject -Property @{
        'parameters' = @(
            @{
                'name' = 'param1'
                'encodedValue' = (ConvertTo-Base64UTF8 -StringToEncode ($inputs.p1))
            },
            @{
                'name' = 'param2'
                'encodedValue' = (ConvertTo-Base64UTF8 -StringToEncode ($inputs.p2))
            },
            @{
                'name' = 'param3'
                'encodedValue' = (ConvertTo-Base64UTF8 -StringToEncode ($inputs.p3))
            }
        )
    }
    return @{ScriptParameters = ($paramObject | Convertto-json)}
}

# adds double quotes around string
function ConvertTo-Base64UTF8($StringToEncode){
    $Bytes = [System.Text.Encoding]::UTF8.GetBytes('"{0}"' -f $StringToEncode)
    return [Convert]::ToBase64String($Bytes)
}
